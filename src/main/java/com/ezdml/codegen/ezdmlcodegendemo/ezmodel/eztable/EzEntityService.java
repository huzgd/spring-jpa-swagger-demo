package com.ezdml.codegen.ezdmlcodegendemo.ezmodel.eztable;

import java.util.List;

public interface EzEntityService {

    List<EzEntity> findAll();
    List<EzEntity> findByIds(Iterable<Long> ids);
    EzEntity findById(Long id);
    long count();

    EzEntity add(EzEntity ezEntity);
    EzEntity update(EzEntity ezEntity);

    void delete(EzEntity ezEntity);
    void deleteById(Long id);
    void deleteByIds(Iterable<Long> ids);

}
