package com.ezdml.codegen.ezdmlcodegendemo.ezmodel.eztable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "EzEntity 相关接口")
@RestController
@RequestMapping("/ez_entity")
public class EzEntityController {

    @Autowired
    private EzEntityService ezEntityService;

    @ApiOperation("获取EzEntity列表")
    @GetMapping("/list")
    public List<EzEntity> list(){
        return ezEntityService.findAll();
    }

    @ApiOperation("获取指定ID的EzEntity")
    @ApiImplicitParam(name = "id", value = "要获取的EzEntity的id" , required = true)
    @GetMapping("/getById")
    public EzEntity getById(Long id) {
        return ezEntityService.findById(id);
    }

    @ApiOperation("获取总数")
    @GetMapping("/count")
    public long count() {
        return ezEntityService.count();
    }

    @ApiOperation("添加新的EzEntity")
    @RequestMapping("/add")
    public EzEntity add(EzEntity ezEntity) {
        return ezEntityService.add(ezEntity);
    }

    @ApiOperation("修改已有的EzEntity")
    @RequestMapping("/update")
    public EzEntity update(EzEntity ezEntity) {
        return ezEntityService.update(ezEntity);
    }

    @ApiOperation("删除指定ID的EzEntity")
    @ApiImplicitParam(name = "id", value = "要删除的EzEntity的id" , required = true)
    @GetMapping("/deleteById")
    public void deleteById(Long id) {
        ezEntityService.deleteById(id);
    }

    @ApiOperation("删除指定ID列表的多个EzEntity")
    @ApiImplicitParam(name = "ids", value = "要删除的EzEntity的id列表" , required = true)
    @RequestMapping("/deleteByIds")
    public void deleteByIds(Iterable<Long> ids) {
        ezEntityService.deleteByIds(ids);
    }

}
