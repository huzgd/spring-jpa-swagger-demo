package com.ezdml.codegen.ezdmlcodegendemo.ezmodel.eztable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EzEntityServiceImpl implements EzEntityService {

    @Autowired
    private EzEntityRepository ezEntityRepository;

    @Override
    public List<EzEntity> findAll() {
        return ezEntityRepository.findAll();
    }

    @Override
    public List<EzEntity> findByIds(Iterable<Long> ids) {
        return ezEntityRepository.findAllById(ids);
    }

    @Override
    public EzEntity findById(Long id) {
        return ezEntityRepository.getById(id);
    }

    @Override
    public long count() {
        return ezEntityRepository.count();
    }

    @Override
    public EzEntity add(EzEntity ezEntity) {
        return ezEntityRepository.save(ezEntity);
    }

    @Override
    public EzEntity update(EzEntity ezEntity) {
        return ezEntityRepository.save(ezEntity);
    }

    @Override
    public void delete(EzEntity ezEntity) {
        ezEntityRepository.delete(ezEntity);
    }

    @Override
    public void deleteById(Long id) {
        ezEntityRepository.deleteById(id);
    }

    @Override
    public void deleteByIds(Iterable<Long> ids) {
        ezEntityRepository.deleteAllById(ids);
    }
}
