package com.ezdml.codegen.ezdmlcodegendemo.ezmodel.eztable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EzEntityRepository  extends JpaRepository<EzEntity,Long> {
}
